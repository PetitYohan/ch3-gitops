{{- define "AppCtx.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-"}}
{{- end }}

{{- define "AppCtx.api" }}
{{- $name := default .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- printf "%s-api" .Release.Name | trunc 63 | trimSuffix "-"}}
{{- end }}

{{- define "AppCtx.bd" }}
{{- $name := default .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- printf "%s-bd" .Release.Name | trunc 63 | trimSuffix "-"}}
{{- end }}

{{- define "AppCtx.front" }}
{{- $name := default .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- printf "%s-front" .Release.Name | trunc 63 | trimSuffix "-"}}
{{- end }}